#!/usr/bin/env python3

import argparse
import yaml
from jinja2 import Environment, FileSystemLoader

parser = argparse.ArgumentParser(description='Map possible SSH connections between hosts')
parser.add_argument("--verbose", '-v', dest="verbose", action='store_true', help="Increase output")
parser.add_argument("infile", type=str, help="SSH map YAML file")
args = parser.parse_args()

# Open and parse the YAML file

with open(args.infile, "r") as stream:
    try:
        data = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

# Check metadata

schemaversion = data['meta']['schemaversion']
if schemaversion != 1:
    print("Unknown schema version {}".format(schemaversion))
    exit(1)

# Basically we're here to invert this hash of hashes. We want to know for
# each key, which user/host combos have the keypair and which user/host
# combos authorise it.

def getkeyid(keystring):
    # Get the key defintion, which is the first two fields
    keydata = keystring.split()
    return ' '.join(keydata[0:2])

keymap = {
  'hosts':  {},
  'keys':   {}
}

def add_key_map(pubkey, host, user, direction):
    keydata = getkeyid(pubkey)

    if keydata not in keymap['keys']:
      keymap['keys'][keydata] = { 'sources': [], 'targets': [] }

    keymap['keys'][keydata][direction].append( [host, user] )

for host in data['hosts']:
    if data['hosts'][host] is None:             # Skip hosts with no users
        continue

    # Make sure host exists in keymap
    dothost = host.replace('.', '_')

    if dothost not in keymap['hosts']:
        keymap['hosts'][dothost] = []

    for user in data['hosts'][host]:
        if data['hosts'][host][user] is None:   # Skip users with no keytypes
            continue
        keymap['hosts'][dothost].append(user)

        if 'public' in data['hosts'][host][user]:
            if data['hosts'][host][user]['public'] is None:
                continue
            for pubkey in data['hosts'][host][user]['public']:
                add_key_map(pubkey, dothost, user, 'sources')
        if 'authorized' in data['hosts'][host][user]:
            if data['hosts'][host][user]['authorized'] is None:
                continue
            for pubkey in data['hosts'][host][user]['authorized']:
                add_key_map(pubkey, dothost, user, 'targets')

# Now parse a template with this data

j2_env = Environment(loader=FileSystemLoader('.'))
print(j2_env.get_template('ssh-mapper.j2').render(keymap))
